import time, json
from dynaconf import settings

from kafka import KafkaConsumer
from kafka.errors import KafkaError
from kafka.admin import KafkaAdminClient, NewTopic

import myLogger
import myDB
import myHelper

myLogger.debug(str("Config variablen: %s"%settings.as_dict()))

consumer = KafkaConsumer(settings.APPCONFIG.ORDERTOPIC, 
                            group_id='orders',
                            auto_offset_reset='earliest',
                            auto_commit_interval_ms=100,
                            bootstrap_servers=settings.APPCONFIG.KAFKASERVERS,
                            value_deserializer=lambda m: json.loads(m.decode('ascii')))

def create_topic():
    admin_client = KafkaAdminClient(bootstrap_servers=settings.APPCONFIG.KAFKASERVERS, client_id='test')
    topic_metadata = admin_client.list_topics()

    if settings.APPCONFIG.ORDERTOPIC not in topic_metadata:
        log.info("Topics werden erstellt")
        topic_list = []
        topic_list.append(NewTopic(name=settings.APPCONFIG.ORDERTOPIC, num_partitions=10, replication_factor=1))
        admin_client.create_topics(new_topics=topic_list, validate_only=False)

def process_order(): 
    for message in consumer:
      val = (message.value["firstname"], 
              message.value["lastname"],
              message.value["orderid"],
              message.value["sent"])
      myDB.insertOrder(val)
      time.sleep(settings.APPCONFIG.ORDERDELAY)


create_topic()

process_order()
