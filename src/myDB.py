import mysql.connector
from dynaconf import settings
import myLogger

mydb = mysql.connector.connect(
            host=settings.MYSQL.host,
            user=settings.MYSQL.user,
            password=settings.MYSQL.password,
            database="mysql"
        ) 

mycursor = mydb.cursor()


def test():
    s="dd"
    myLogger.info("Starte DB Verbindung %s"%s)
    


def createDB():

    mycursor.execute("SHOW DATABASES")

    f=[]
    for x in mycursor:
        f.append(x[0])

    myLogger.info("Datenbanken: %s"%f)


    if "orders" in f:
        myLogger.info("DB existiert. Nichts zu machen")
    else:
        myLogger.info("DB existiert nicht. Wird angelegt.")
        mycursor.execute("CREATE DATABASE orders")
        mycursor.execute("""CREATE TABLE orders.orders (
            id INT NOT NULL AUTO_INCREMENT,
            firstname VARCHAR(45) NULL,
            lastname VARCHAR(45) NULL,
            orderid VARCHAR(45) NULL,
            sent VARCHAR(45) NULL,
            PRIMARY KEY (id)
        );""")

def insertOrder(val):
    sql = "insert into orders.orders (firstname, lastname, orderid, sent) VALUES (%s, %s, %s, %s)"    
    mycursor.execute(sql, val)
    mydb.commit()
    myLogger.info(str("DB Eintrag geschrieben: %s"%(str(val))))

createDB()